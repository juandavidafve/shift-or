import {
  Routes,
  Route,
  HashRouter,
} from "react-router-dom";
import Playground from "./pages/playground/Playground";
import Complexity from "./pages/complexity/Complexity";
import Algorithm from "./pages/algorithm/Algorithm";
import Header from "./components/Header";
import Footer from "./components/Footer";
import getLocalStorageTheme from "./hooks/getLocalStorageTheme";

export default function App() {
  const [appTheme, setAppTheme] = getLocalStorageTheme();

  return (
    <HashRouter className>
      <div className={`${appTheme} transition dark:bg-zinc-950 dark:text-zinc-100 text-slate-950 bg-slate-100 flex flex-col justify-between min-h-screen`}>
        <Header appTheme={appTheme} setAppTheme={setAppTheme} />
        <main className="w-11/12 mx-auto max-w-screen-2xl">
          <Routes>
            <Route path="/" element={<Playground />} />
            <Route path="/complexity" element={<Complexity />} />
            <Route path="/algorithm" element={<Algorithm />} />
          </Routes>
        </main>
        <Footer />
      </div>
    </HashRouter>
  );
}