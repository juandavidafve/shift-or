export default function getExecution(text, pattern) {
    const steps = [];

    shiftOrSearch(text, pattern, steps);

    return steps;
}

// Algoritmo ShiftOr adaptado de:
// https://iq.opengenus.org/shift-or-algorithm-for-string-matching/
function shiftOrSearch(text, pattern, steps) {
    steps.push({
        line: 1,
        explanation: "Se invoca el método shiftOrSearch(text, pattern).",
        bitmask: null,
        matches: [],
        loopText: -1,
        loopPattern: -1,
    });
    steps.push({
        line: 2,
        explanation: "Se crea el arreglo de resultados.",
        bitmask: null,
        matches: [],
        loopText: -1,
        loopPattern: -1,
    });
    const results = [];

    // No se puede trabajar con patrones vacíos
    steps.push({
        line: 4,
        explanation: "Verificar si el patrón está vacío.",
        bitmask: null,
        matches: [],
        loopText: -1,
        loopPattern: -1,
    });
    if (pattern.length === 0) {
        console.error("Patrón vacío.");
        steps.push({
            line: 5,
            explanation: "En este caso, el patrón está vacío, así que se lanza el mensaje de error.",
            bitmask: null,
            matches: [],
            loopText: -1,
            loopPattern: -1,
        });
        steps.push({
            line: 6,
            explanation: "Salir del método.",
            bitmask: null,
            matches: [],
            loopText: -1,
            loopPattern: -1,
        });
        return results;
    }

    // El algoritmo no soporta patrones de más de 31 caracteres
    steps.push({
        line: 9,
        explanation: "Verificar si el patrón excede la longitud máxima de 31 caracteres.",
        bitmask: null,
        matches: [],
        loopText: -1,
        loopPattern: -1,
    });
    if (pattern.length > 31) {
        steps.push({
            line: 10,
            explanation: `En este caso, el patrón es de ${pattern.length} caracteres, así que se lanza el mensaje de error.`,
            bitmask: null,
            matches: [],
            loopText: -1,
            loopPattern: -1,
        });
        console.error("Patrón demasiado largo.");
        steps.push({
            line: 11,
            explanation: "Salir del método.",
            bitmask: null,
            matches: [],
            loopText: -1,
            loopPattern: -1,
        });
        return undefined;
    }

    steps.push({
        line: 14,
        explanation: `Llamar al método createBitMaskArr(pattern)`,
        bitmask: null,
        matches: [],
        loopText: -1,
        loopPattern: -1,
    });
    const bitmask = createBitMaskArr(pattern, steps);
    steps.push({
        line: 15,
        explanation: "Se crea una variable patternMask para almacenar las máscaras de bits creadas por el método createBitMaskArr(pattern).",
        bitmask: {
            patternMask: [...bitmask[0]],
            patternIndex: {...bitmask[1]}
        },
        matches: [...results],
        loopText: -1,
        loopPattern: -1,
    });
    const patternMask = bitmask[0];
    steps.push({
        line: 16,
        explanation: "Se crea una variable patternIndex para almacenar los índices de las máscaras de bits creadas por el método createBitMaskArr(pattern).",
        bitmask: {
            patternMask: [...bitmask[0]],
            patternIndex: {...bitmask[1]}
        },
        matches: [...results],
        loopText: -1,
        loopPattern: -1,
    });
    const patternIndex = bitmask[1];

    steps.push({
        line: 18,
        explanation: "Se recorre cada uno de los caracteres del texto.",
        bitmask: {
            patternMask: [...patternMask],
            patternIndex: {...patternIndex}
        },
        matches: [...results],
        loopText: -1,
        loopPattern: -1,
    });
    for (let i = 0; i < text.length; i++) {
        steps.push({
            line: 19,
            explanation: "Se obtiene el carácter actual.",
            bitmask: {
                patternMask: [...patternMask],
                patternIndex: {...patternIndex}
            },
            matches: [...results],
            loopText: i,
            loopPattern: -1,
        });
        const char = text[i];

        // Obtener el índice de la máscara de bits
        steps.push({
            line: 21,
            explanation: `Se obtiene el índice de la máscara de bits correspondiente al carácter ${char}.`,
            bitmask: {
                patternMask: [...patternMask],
                patternIndex: {...patternIndex}
            },
            matches: [...results],
            loopText: i,
            loopPattern: -1,
        });
        let bitmaskIndex = patternIndex[char];
        steps.push({
            line: 22,
            explanation: `Se verifica si el índice a buscar no se encontró`,
            bitmask: {
                patternMask: [...patternMask],
                patternIndex: {...patternIndex}
            },
            matches: [...results],
            loopText: i,
            loopPattern: -1,
        });
        if (bitmaskIndex === undefined) {
            steps.push({
                line: 23,
                explanation: `En este caso, no existe una máscara de bits para el carácter ${char}, así que se define el índice como 1, para obtener la máscara de bits por defecto.`,
                bitmask: {
                    patternMask: [...patternMask],
                    patternIndex: {...patternIndex}
                },
                matches: [...results],
                loopText: i,
                loopPattern: -1,
            });
            bitmaskIndex = 1;
        }

        // Operaciones de bits
        steps.push({
            line: 26,
            explanation: `Se realiza un OR entre el patrón actual, que en este caso es ${patternMask[0].toString(2)}, y la máscara de bits del carácter ${char}, el cual es ${patternMask[bitmaskIndex].toString(2)}.`,
            bitmask: {
                patternMask: [...patternMask],
                patternIndex: {...patternIndex}
            },
            matches: [...results],
            loopText: i,
            loopPattern: -1,
        });
        patternMask[0] |= patternMask[bitmaskIndex];
        steps.push({
            line: 27,
            explanation: `Se realiza un corrimiento a la izquierda en 1 posición.`,
            bitmask: {
                patternMask: [...patternMask],
                patternIndex: {...patternIndex}
            },
            matches: [...results],
            loopText: i,
            loopPattern: -1,
        });
        patternMask[0] <<= 1;

        // Match
        steps.push({
            line: 29,
            explanation: `Se verifica si hubo un match. Esto se hace comprobando que el bit número ${pattern.length}, contando de derecha a izquierda, sea 0.`,
            bitmask: {
                patternMask: [...patternMask],
                patternIndex: {...patternIndex}
            },
            matches: [...results],
            loopText: i,
            loopPattern: -1,
        });
        if ((patternMask[0] & (1 << pattern.length)) == 0) {
            steps.push({
                line: 30,
                explanation: `En este caso, hubo un match, así que se agrega al arreglo de resultados la posición de la subcadena, la cual corresponde a i - tamaño del patrón + 1.`,
                bitmask: {
                    patternMask: [...patternMask],
                    patternIndex: {...patternIndex}
                },
                matches: [...results],
                loopText: i,
                loopPattern: -1,
            });
            results.push(i - pattern.length + 1)
        }
    }

    steps.push({
        line: 34,
        explanation: `Se retorna el arreglo de posiciones en donde se encontró el substring. En total se hallaron ${results.length} patrones dentro del texto.`,
        bitmask: {
            patternMask: [...patternMask],
            patternIndex: {...patternIndex}
        },
        matches: [...results],
        loopText: -1,
        loopPattern: -1,
    });
    return results;
}

function createBitMaskArr(pattern, steps) {
    steps.push({
        line: 37,
        explanation: "Se invoca el método createBitMaskArr(pattern).",
        bitmask: null,
        matches: [],
        loopText: -1,
        loopPattern: -1,
    });

    steps.push({
        line: 38,
        explanation: "Se crea el arreglo patternIndex, el cual funciona como un map, donde la clave es un carácter, y el valor es el índice de su respectiva máscara de bits en patternMask",
        bitmask: null,
        matches: [],
        loopText: -1,
        loopPattern: -1,
    });
    const patternIndex = []; // Map que almacena [caracter] -> indice en patternMask
    steps.push({
        line: 39,
        explanation: "Se crea el arreglo patternMask, el cual va a contener las máscaras de bits de los caracteres. La posición 0 va a manejar la máscara de bits actual, lo que en este algoritmo llamamos R. La posición 1 va a contener la máscara de bits predeterminada en caso de exista un carácter en el texto que no esté en el patrón. Hay que tener en cuenta que se usa Uint32Array para crear el arreglo, ya que de esta forma podemos trabajar en JavaScript con un arreglo de números sin signo de 32 bits.",
        bitmask: null,
        matches: [],
        loopText: -1,
        loopPattern: -1,
    });
    const patternMask = new Uint32Array(pattern.length + 2); // Se usa Uint32Array para poder manejar enteros unsigned

    steps.push({
        line: 41,
        explanation: "Se crea la máscara de 32 bits inicial para patternMask[0] el cual representa a R.",
        bitmask: {
            patternMask: [...patternMask],
            patternIndex: {...patternIndex}
        },
        matches: [],
        loopText: -1,
        loopPattern: -1,
    });
    patternMask[0] = ~1; // patternMask[0]: Va a ser R, para manejar el estado actual
    steps.push({
        line: 42,
        explanation: "Se crea la máscara de 32 bits inicial para patternMask[1] el cual contiene la máscara predeterminada.",
        bitmask: {
            patternMask: [...patternMask],
            patternIndex: {...patternIndex}
        },
        matches: [],
        loopText: -1,
        loopPattern: -1,
    });
    patternMask[1] = ~0; // patternMask[1]: Valor predeterminado en caso de que una letra del texto no esté en el patron

    // patternMask[2...m]: Va a contener los mapas de bits de los caracteres del patron
    steps.push({
        line: 44,
        explanation: "Se recorre cada uno de los caracteres del patrón.",
        bitmask: {
            patternMask: [...patternMask],
            patternIndex: {...patternIndex}
        },
        matches: [],
        loopText: -1,
        loopPattern: -1,
    });
    for (let i = 0; i < pattern.length; i++) {
        steps.push({
            line: 45,
            explanation: `Se obtiene el carácter actual, en este caso es ${pattern[i]}`,
            bitmask: {
                patternMask: [...patternMask],
                patternIndex: {...patternIndex}
            },
            matches: [],
            loopText: -1,
            loopPattern: i,
        });
        const char = pattern[i];

        steps.push({
            line: 47,
            explanation: `Verificar si el carácter ya se encuentra indexado en patternIndex.`,
            bitmask: {
                patternMask: [...patternMask],
                patternIndex: {...patternIndex}
            },
            matches: [],
            loopText: -1,
            loopPattern: i,
        });
        if (patternIndex[char] === undefined) {
            steps.push({
                line: 48,
                explanation: `En este caso, no se encuentra indexado. Entonces se asigna la llave ${char} con un valor ${i + 2}. Recordar que se debe sumar 2 a la posición, porque las dos primeras posiciones ya se encuentran asignadas.`,
                bitmask: {
                    patternMask: [...patternMask],
                    patternIndex: {...patternIndex}
                },
                matches: [],
                loopText: -1,
                loopPattern: i,
            });
            patternIndex[char] = i + 2;
            steps.push({
                line: 49,
                explanation: `Se crea la máscara de bits y se almacena en patternMask, en la posición ${i + 2}.`,
                bitmask: {
                    patternMask: [...patternMask],
                    patternIndex: {...patternIndex}
                },
                matches: [],
                loopText: -1,
                loopPattern: i,
            });
            patternMask[i + 2] = ~0;
        }

        steps.push({
            line: 52,
            explanation: `Se realizan una serie de operaciones de bits. Primero se corre a la izquierda una posición, luego se realiza una negación. Finalmente se realiza un AND con su valor inicial, que es ${patternMask[patternIndex[char]].toString(2)}.`,
            bitmask: {
                patternMask: [...patternMask],
                patternIndex: {...patternIndex}
            },
            matches: [],
            loopText: -1,
            loopPattern: i,
        });
        patternMask[patternIndex[char]] &= ~(1 << i);
    }

    steps.push({
        line: 55,
        explanation: `Se retorna un arreglo de dos posiciones. La primera posición contiene el arreglo de máscaras de bits, y la segunda posición contiene el map de los caracteres con sus respectivos índices.`,
        bitmask: {
            patternMask: [...patternMask],
            patternIndex: {...patternIndex}
        },
        matches: [],
        loopText: -1,
        loopPattern: -1,
    });
    return [
        patternMask, patternIndex
    ]
}