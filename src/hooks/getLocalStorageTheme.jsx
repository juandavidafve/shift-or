import { useEffect, useState } from "react";

export default function getLocalStorageTheme(){
  const [appTheme, setAppTheme] = useState("light");

  useEffect(() => {
    let theme = localStorage.getItem("theme");
    if (!theme) {
      if (window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches) {
        theme = "dark";
      } else {
        theme = "light";
      }

      localStorage.setItem("theme", theme);
    }

    setAppTheme(theme);
  }, []);

  return [appTheme, setAppTheme];
}