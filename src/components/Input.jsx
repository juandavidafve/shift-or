import { useEffect } from "react";
import { useState } from "react";

export default function Input({
    type = "text",
    name = "",
    label = "",
    subText = "",
    disabled = false,
    highlightLoop = -1,
    highlightMatches = [],
    patternLength = 0,
    setParentValue = () => { },
}) {
    const [value, setValue] = useState("");
    const inputClasses = "p-2 rounded-md w-full box-border font-mono dark:bg-zinc-900 bg-slate-200 focus:outline outline-slate-400 dark:outline-zinc-800 transition";
    const matches = [];

    for(const m of highlightMatches){
        for(let i = 0; i < patternLength; i++){
            const val = m + i;
            if(matches.length === 0 || matches[matches.length - 1] < val){
                matches.push(val);
            }
        }
    }

    useEffect(() => {
        setParentValue(value);
    }, [value])

    let charPosition = -1;
    return (
        <div className="mb-5">
            <label htmlFor={name} className="block font-bold mb-2">{label}</label>
            {
                !disabled ?
                    type === "text" ?
                        <input
                            id={name}
                            name={name}
                            type={type}
                            value={value}
                            onChange={(e) => setValue(e.target.value)}
                            className={inputClasses}
                        ></input>
                        :
                        <textarea
                            id={name}
                            name={name}
                            type={type}
                            value={value}
                            onChange={(e) => setValue(e.target.value)}
                            className={inputClasses}
                        ></textarea>
                    : <div className={inputClasses}>
                        {value.split("\n").map((line, i) => {
                            charPosition++;
                            return (
                                <div key={i}>
                                    {line.split("").map((char, j) => {
                                        let styles = "";

                                        if(charPosition === matches[0]){
                                            matches.shift();
                                            styles = "dark:bg-emerald-600 bg-emerald-500"
                                        }

                                        if(charPosition === highlightLoop){
                                            styles = "dark:bg-amber-600 bg-amber-500"
                                        }

                                        charPosition++;
                                        return <span key={j} className={styles}>{char}</span>
                                    })}
                                </div>
                            )
                        })}
                    </div>
            }

            <span className="block font-light">{subText}</span>
        </div>
    )
}