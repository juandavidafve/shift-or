import logoSistemas from "../assets/img/ing-sistemas.png";
import logoUFPS from "../assets/img/ufps.png";

export default function Footer() {
    const linkStyle = "inline-block text-emerald-600 hover:underline hover:scale-105 underline-offset-2 decoration-2 transition";

    return (
        <footer class="dark:bg-zinc-900 bg-slate-200 py-6">
            <div className="w-11/12 mx-auto max-w-screen-2xl flex items-center justify-evenly flex-col gap-10 sm:gap-4 sm:flex-row lg:justify-between">
                <div className="flex flex-col gap-5 lg:flex-row lg:gap-16">
                    <p className="text-center lg:text-left">Desarrollador Por:</p>
                    <div className="text-center">
                        <a className={`font-bold ${linkStyle}`} href="https://gitlab.com/juandavidafve" target="_blank">Juan David Afanador Verjel</a>
                        <p>Código: 1152247</p>
                    </div>
                    <div className="text-center">
                        <a className={`font-bold ${linkStyle}`} href="https://gitlab.com/yieison" target="_blank">Yieison Lizarazo</a>
                        <p>Código: 1151938</p>
                    </div>
                </div>
                <div className="flex items-center gap-2">
                    <a href="https://ingsistemas.cloud.ufps.edu.co/" target="_blank">
                        <img className="max-w-20 hover:scale-105 transition" src={logoSistemas} />
                    </a>
                    <a href="https://ww2.ufps.edu.co/" target="_blank">
                        <img className="max-w-16 hover:scale-105 transition" src={logoUFPS} />
                    </a>

                </div>
            </div>
        </footer>
    )
}