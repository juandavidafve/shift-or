export default function Button({
    className = "",
    disabled = false,
    action = () => { },
    children
}) {
    return (
        <button
            onClick={action}
            disabled={disabled}
            className={`${className} font-bold text-zinc-100 dark:bg-emerald-600 bg-emerald-500 py-2 px-6 rounded-md transition ring-emerald-900 hover:bg-emerald-700 active:bg-emerald-800 enabled:hover:ring dark:disabled:bg-emerald-950 disabled:bg-emerald-100 dark:disabled:text-zinc-100 disabled:text-emerald-400`}
        >
            {children}
        </button>
    )
}