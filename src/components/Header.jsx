import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMoon, faSun } from "@fortawesome/free-solid-svg-icons";
import { useState } from "react";
import Button from "./Button";
import toggleTheme from "../hooks/toggleTheme";
import "../assets/css/header.css"


export default function Header({ appTheme = "light", setAppTheme = () => { } }) {
    const [menu, setMenu] = useState(false);
    const linkStyle = "hover:text-emerald-600 hover:underline hover:scale-105 underline-offset-8 decoration-2 transition";

    return (
        <header className="dark:bg-zinc-900 bg-slate-200 py-4">
            <div className="w-11/12 mx-auto max-w-screen-2xl flex flex-col items-end">
                <Button className="md:hidden" action={() => setMenu(!menu)}>
                    <svg className="w-5 h-5" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 17 14">
                        <path stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M1 1h15M1 7h15M1 13h15" />
                    </svg>
                </Button>

                <div className={`md:flex md:items-center md:justify-between w-full overflow-hidden md:overflow-visible transition-all duration-200 p-2 ${!menu ? "max-h-0 md:max-h-screen" : "max-h-screen"}`}>
                    <nav className="flex items-center justify-center md:justify-end my-16 md:my-0 font-bold text-center">
                        <ul className="flex flex-col md:flex-row gap-6 md:gap-10">
                            <li className={linkStyle}>
                                <Link to="/">Playground</Link>
                            </li>
                            <li className={linkStyle}>
                                <Link to="/algorithm">Algoritmo</Link>
                            </li>
                            <li className={linkStyle}>
                                <Link to="/complexity">Complejidad</Link>
                            </li>
                        </ul>
                    </nav>

                    <Button className="w-full md:w-auto" action={() => toggleTheme(appTheme, setAppTheme)}>
                        <FontAwesomeIcon icon={appTheme === "dark" ? faSun : faMoon} />
                    </Button>
                </div>
            </div>
        </header >
    )
}