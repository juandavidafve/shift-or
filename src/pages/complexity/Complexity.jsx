import PDFAnalisis from "./assets/1152247-1151938.pdf"

export default function Complexity() {

  return (
    <div className="container mx-auto px-4 py-8">
      <h1 className="text-center font-bold text-2xl my-6">Complejidad del Algoritmo Shift Or</h1>
      <div className="relative pb-[177.77%] md:pb-[56.25%] h-0 overflow-hidden mb-8">
        <iframe
          className="absolute top-0 left-0 w-full h-full"
          src={PDFAnalisis}
          title="Algoritmo Shift Or"
          allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
          allowFullScreen
        ></iframe>
      </div>
    </div>
  );
}
