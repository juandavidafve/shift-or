import React from 'react';

const Algorithm = () => {
  return (
    <div className="container mx-auto px-4 py-8">
      <h1 className="text-center font-bold text-2xl my-6">Video Algoritmo Shift Or</h1>
      <p className="text-lg my-4">
        El algoritmo Shift Or es una técnica eficiente para la búsqueda de patrones en textos.
        Este video explica su funcionamiento y aplicaciones.
      </p>
      <div className="max-w-3xl mx-auto">
        <div className="relative pb-[56.25%] h-0 overflow-hidden mb-8">
          <iframe
            className="absolute top-0 left-0 w-full h-full"
            src="https://www.youtube.com/embed/blVhDuSoLAs"
            title="Algoritmo Shift Or"
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen
          ></iframe>
        </div>
      </div>
    </div>
  );
};

export default Algorithm;