import { useEffect, useRef } from "react";
import SyntaxHighlighter from 'react-syntax-highlighter';
import shiftOrSearch from "../assets/shiftOrSearch.txt?raw";
import "../assets/css/CodeViewer.css";

export default function CodeViewer({
  step = null
}) {
  const lineRef = useRef();
  const parentRef = useRef();

  function scrollToLine() {
    const line = lineRef.current;
    const parent = parentRef.current;

    if (line && parent) {
      parent.scroll({
        top: line.offsetTop,
        behavior: "smooth"
      })
    }
  }

  function highlightLine(lineNumber) {
    let className = "block ";
    let shouldHighlight = step && step.line === lineNumber

    if (shouldHighlight) {
      className += "bg-emerald-300 dark:bg-emerald-900"
    }

    return {
      class: className,
      ref: shouldHighlight ? lineRef : null
    }
  }

  useEffect(() => {
    scrollToLine();
  }, [step])

  return (
    <div>
      <h2 className="font-bold mb-2">Ejecución</h2>
      <div className="dark:bg-zinc-900 bg-slate-200 rounded-md">
        {
          step
            ? <div
              className="dark:bg-emerald-900 bg-emerald-600 text-zinc-100 p-4 rounded-md"
            >{step.explanation}</div>
            : ""
        }
        <div ref={parentRef} className="relative max-h-96 overflow-auto mt-4">
          <SyntaxHighlighter
            language="javascript"
            showLineNumbers={true}
            wrapLines
            useInlineStyles={false}
            lineProps={highlightLine}
            className="rounded w-full min-w-fit">
            {shiftOrSearch}
          </SyntaxHighlighter>
          {
            step ? <div className="h-80"></div> : ""
          }
        </div>
      </div>
    </div>
  )
}