export default function BitsTable({
    bitmask = null
}) {

    const cellStyle = "border-2 border-zinc-600 p-1";
    const valueCellStyle = `${cellStyle} font-mono text-center tracking-widest`;

    return (
        bitmask ?
            <div className="mt-4">
                <h2 className="font-bold mb-2">Tabla de Máscara de bits</h2>
                <div  className="dark:bg-zinc-900 bg-slate-200 p-4 rounded">
                    <table className="w-full border-2">
                        <thead>
                            <tr>
                                <th scope="col" className={cellStyle}>Carácter</th>
                                <th scope="col" className={cellStyle}>Bits</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                Object.keys(bitmask.patternIndex).map((char, index) => {
                                    return (
                                        <tr key={index}>
                                            <th scope="col" className={cellStyle}>{char}</th>
                                            <td className={valueCellStyle}>{bitmask.patternMask[bitmask.patternIndex[char]].toString(2)}</td>
                                        </tr>
                                    )
                                })
                            }
                            <tr>
                                <th scope="col" className={cellStyle}>Default</th>
                                <td className={valueCellStyle}>{bitmask.patternMask[1].toString(2)}</td>
                            </tr>
                            <tr>
                                <th scope="col" className={cellStyle}>Current</th>
                                <td className={valueCellStyle}>{bitmask.patternMask[0].toString(2)}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            : ""

    )
}