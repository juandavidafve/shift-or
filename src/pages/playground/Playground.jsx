import { useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBackwardFast, faBackwardStep, faForwardFast, faForwardStep, faPlay, faRotateRight } from "@fortawesome/free-solid-svg-icons";
import getExecution from "../../backend/getExecution"
import Input from "../../components/Input";
import Button from "../../components/Button";
import CodeViewer from "./components/CodeViewer";
import BitsTable from "./components/BitsTable";

export default function Playground() {
  const [text, setText] = useState("");
  const [pattern, setPattern] = useState("");
  const [steps, setSteps] = useState([]);
  const [currentStep, setCurrentStep] = useState(-1);

  function next(nSteps = 1) {
    if (currentStep + nSteps < steps.length) {
      setCurrentStep(currentStep + nSteps);
    } else {
      setCurrentStep(steps.length - 1);
    }
  }

  function previous(nSteps = 1) {
    if (currentStep - nSteps >= 0) {
      setCurrentStep(currentStep - nSteps);
    } else {
      setCurrentStep(0);
    }
  }

  return (
    <div className="my-10">
      <h1 className="text-center font-bold text-2xl my-6">Playground Algoritmo Shift Or</h1>
      <div className="grid grid-cols-1 lg:grid-cols-2 gap-6">
        <div>
          <Input
            type="text"
            label="Patrón"
            name="pattern"
            subText={
              currentStep === -1 ? `${pattern.length}/31 Carácteres` : ""
            }
            setParentValue={setPattern}
            disabled={currentStep >= 0}
            highlightLoop={currentStep >= 0 ? steps[currentStep].loopPattern : null}
            patternLength={pattern.length}
          />
          <Input
            type="textarea"
            label="Texto"
            name="text"
            subText={
              currentStep >= 0 ? `${steps[currentStep].matches.length} Resultados` : ""
            }
            setParentValue={setText}
            disabled={currentStep >= 0}
            highlightMatches={currentStep >= 0 ? steps[currentStep].matches : []}
            highlightLoop={currentStep >= 0 ? steps[currentStep].loopText : null}
            patternLength={pattern.length}
          />
          {
            currentStep === -1
              ? <Button action={() => {
                setSteps(getExecution(text, pattern))
                setCurrentStep(0);
              }} className="flex items-center gap-2">
                <FontAwesomeIcon icon={faPlay} />
                Iniciar
              </Button>
              : <>
                <div className="flex justify-between mt-4 flex-col md:flex-row">
                  <div>
                    <div className="grid grid-cols-4 gap-4">
                      <Button action={() => previous(10)} disabled={currentStep === 0}>
                        <FontAwesomeIcon icon={faBackwardFast} />
                      </Button>
                      <Button  action={() => previous(1)} disabled={currentStep === 0}>
                        <FontAwesomeIcon icon={faBackwardStep} />
                      </Button>
                      <Button action={() => next(1)} disabled={currentStep === steps.length - 1}>
                        <FontAwesomeIcon icon={faForwardStep} />
                      </Button>
                      <Button action={() => next(10)} disabled={currentStep === steps.length - 1}>
                        <FontAwesomeIcon icon={faForwardFast} />
                      </Button>
                    </div>
                    <p className="text-center my-2 font-light">{`${currentStep + 1}/${steps.length}`}</p>
                  </div>
                  <div>
                    <Button value="Reiniciar" action={() => {
                      setSteps([]);
                      setCurrentStep(-1);
                    }} className="w-full md:w-auto">
                      <FontAwesomeIcon icon={faRotateRight} />
                    </Button>
                  </div>
                </div>
                <BitsTable bitmask={steps[currentStep].bitmask} />
              </>
          }

        </div>
        <CodeViewer step={steps[currentStep]} />
      </div>
    </div>
  )
}
